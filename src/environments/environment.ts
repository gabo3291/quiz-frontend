// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebaseConfig: {
    apiKey: 'AIzaSyBWLcqix0FHehYQSpyuXppH4UvGVo6HMMo',
    authDomain: 'portfolio-f89f8.firebaseapp.com',
    databaseURL: 'https://portfolio-f89f8.firebaseio.com',
    projectId: 'portfolio-f89f8',
    storageBucket: 'portfolio-f89f8.appspot.com',
    messagingSenderId: '888550626866',
    appId: '1:888550626866:web:3d5bec6cb95e5bfe2fce2b'
  },
  urlUsersApi: 'http://localhost:8080/api/v1/users/',
  urlBulletinsApi: 'http://localhost:9090/api/v1',
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
