import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizBulletinUploadTaskComponent } from './quiz-bulletin-upload-task.component';

describe('QuizBulletinUploadTaskComponent', () => {
  let component: QuizBulletinUploadTaskComponent;
  let fixture: ComponentFixture<QuizBulletinUploadTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizBulletinUploadTaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizBulletinUploadTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
