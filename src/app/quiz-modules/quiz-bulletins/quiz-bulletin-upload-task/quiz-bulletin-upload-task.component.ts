import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import {AngularFirestore} from '@angular/fire/firestore';

import {Observable} from 'rxjs';
import {finalize, tap} from 'rxjs/operators';

import * as uuid from 'uuid';
import {QuizBulletinImageService} from '../../../quiz-services/quiz-services';

@Component({
  selector: 'quiz-bulletin-upload-task',
  templateUrl: './quiz-bulletin-upload-task.component.html',
  encapsulation: ViewEncapsulation.None
})
export class QuizBulletinUploadTaskComponent implements OnInit {

  @Input() file: File | undefined;

  public task: AngularFireUploadTask | undefined;

  public percentage: Observable<number | undefined> | undefined;
  public snapshot: Observable<any> | undefined;
  public downloadURL: any;

  constructor(
    private _storage: AngularFireStorage,
    private _db: AngularFirestore,
    private _quizBulletinImage: QuizBulletinImageService,
  ) { }

  ngOnInit(): void {
    this.startUpload();
  }

  public startUpload(): void {
    // The storage path
    const name = `${Date.now()}_${uuid.v4()}`;
    const path = `test/${name}`;
    // Reference to storage bucket
    const ref = this._storage.ref(path);
    // The main task
    this.task = this._storage.upload(path, this.file);

    // Progress monitoring
    this.percentage = this.task.percentageChanges();

    this.snapshot   = this.task.snapshotChanges().pipe(
      tap(item => console.log({item})),
      // The file's download URL
      finalize( async () =>  {
        this.downloadURL = await ref.getDownloadURL().toPromise();
        this._quizBulletinImage.imageUploadSuccessful(this.downloadURL);
        this._db.collection('files').add( { downloadURL: this.downloadURL, path });
      }),
    );
  }

}
