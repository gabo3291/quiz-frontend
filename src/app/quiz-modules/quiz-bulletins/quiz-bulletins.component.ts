import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {QuizBulletinResponseModel, QuizUserModel} from '../../quiz-models';
import {QuizBulletinsFakeService} from '../../quiz-services/quiz-data-fake/quiz-bulletins-fake.service';
import {QuizBulletinFormComponent} from './quiz-bulletin-form/quiz-bulletin-form.component';
import {QuizBulletinsService} from '../../quiz-services/quiz-api';

@Component({
  selector: 'quiz-quiz-bulletins-page',
  templateUrl: './quiz-bulletins.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class QuizBulletinsComponent implements OnInit, OnDestroy {
  public bulletins: QuizBulletinResponseModel[] = [];
  public userData: QuizUserModel | undefined;
  private _subscribe  = new Subject();

  constructor(
    private _cdRed: ChangeDetectorRef,
    private _quizFakeBulletins: QuizBulletinsFakeService,
    private _quizBulletins: QuizBulletinsService,
    public dialog: MatDialog
  ) {
    this.getUserData();
  }

  public ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this._subscribe.next();
    this._subscribe.complete();
  }

  public init(): void {
    this._quizFakeBulletins.listenReloadData
      .pipe(takeUntil(this._subscribe))
      .subscribe(() => {
        this._quizBulletins.getAllBulletins().subscribe(response => {
          this._quizFakeBulletins.updateListBulletins(response);
        });
    });
    this._quizFakeBulletins.bulletins$
      .pipe(takeUntil(this._subscribe))
      .subscribe((response: QuizBulletinResponseModel[]) => {
        this.bulletins = response;
        this._cdRed.detectChanges();
      });
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(QuizBulletinFormComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  private getUserData(): void {
    const userLocalStorage = localStorage.getItem('user');
    if (userLocalStorage) {
      this.userData = JSON.parse(userLocalStorage);
    }
  }

}
