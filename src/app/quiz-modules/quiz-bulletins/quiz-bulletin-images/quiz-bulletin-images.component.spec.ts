import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizBulletinImagesComponent } from './quiz-bulletin-images.component';

describe('QuizBulletinImagesComponent', () => {
  let component: QuizBulletinImagesComponent;
  let fixture: ComponentFixture<QuizBulletinImagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizBulletinImagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizBulletinImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
