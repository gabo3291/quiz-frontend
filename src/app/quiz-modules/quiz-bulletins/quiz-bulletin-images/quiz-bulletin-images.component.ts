import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {QuizAttachmentResponseModel} from '../../../quiz-models';

@Component({
  selector: 'quiz-bulletin-images',
  templateUrl: './quiz-bulletin-images.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class QuizBulletinImagesComponent {
  @Input() attachments: QuizAttachmentResponseModel[] | undefined;
}
