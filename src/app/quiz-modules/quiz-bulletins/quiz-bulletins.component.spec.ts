import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizBulletinsComponent } from './quiz-bulletins.component';

describe('QuizBulletinsPageComponent', () => {
  let component: QuizBulletinsComponent;
  let fixture: ComponentFixture<QuizBulletinsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizBulletinsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizBulletinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
