import { NgModule } from '@angular/core';
import {QuizBulletinsRoutingModule} from './quiz-bulletins-routing.module';
import {QuizBulletinsFakeService} from '../../quiz-services/quiz-data-fake/quiz-bulletins-fake.service';
import {QuizBulletinsService, QuizCommentsService} from '../../quiz-services/quiz-api';
import {QuizBulletinImageService} from '../../quiz-services/quiz-services';


@NgModule({
  imports: [
    QuizBulletinsRoutingModule
  ],
  providers: [
    QuizBulletinsFakeService,
    QuizBulletinImageService,
    QuizBulletinsService,
    QuizCommentsService
  ]
})
export class QuizBulletinsModule { }
