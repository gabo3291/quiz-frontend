import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {forkJoin, Subject} from 'rxjs';
import {QuizBulletinResponseModel, QuizCommentResponseModel} from '../../../quiz-models';
import {QuizCommentsService} from '../../../quiz-services/quiz-api';
import {QuizReplyCommentFormComponent} from '../quiz-reply-comment-form/quiz-reply-comment-form.component';


@Component({
  selector: 'quiz-bulletin',
  templateUrl: './quiz-bulletin.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class QuizBulletinComponent implements OnInit, OnDestroy {

  @Input() bulletin: QuizBulletinResponseModel | null = null;
  public currentCommentId = -1;
  public currentPositionComment = -1;
  public comments: QuizCommentResponseModel[] = [];
  public formComment: FormGroup;
  private subscriber: Subject<QuizCommentResponseModel> = new Subject<QuizCommentResponseModel>();
  constructor(
    public dialog: MatDialog,
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _quizCommentService: QuizCommentsService
  ) {
    this.formComment = this._fb.group({
      content: ['', Validators.required]
    });
  }

  public ngOnInit(): void {
    this.init();
  }

  public ngOnDestroy(): void {
    this.subscriber.next();
    this.subscriber.complete();
  }

  public submitComment(): void {
    if (this.formComment.valid) {
      this._quizCommentService
        .saveComment(this.formComment.value.content, this.id)
        .subscribe((response: QuizCommentResponseModel) => {
          this.comments.push(response);
          this.formComment.reset();
          this._cd.markForCheck();
        });
    }
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(
      QuizReplyCommentFormComponent, {width: '420px', data: {id: this.currentCommentId}}
    );

    dialogRef.afterClosed().subscribe((result: any) => {
      // @ts-ignore
      this.comments[this.currentPositionComment].replies.push(result);
      this._cd.markForCheck();
    });
  }

  private init(): void {
    this._quizCommentService
      .getCommentsByBulletinId(this.id)
      .subscribe((response: QuizCommentResponseModel[]) => {
        this.comments = response;
        this.getReplyComments();
      });
  }

  private getReplyComments(): void {
    const routes: any[] = [];
    for (const comment of this.comments) {
      routes.push(this._quizCommentService.getRepliesByCommentId(comment.id));
    }
    forkJoin(routes).subscribe((response: any) => {
      this.putRepliesOnComment(response);
    });
  }
  private putRepliesOnComment(allReplies: Array<QuizCommentResponseModel[]>): void {
    for (let i = 0; i < this.comments.length; i++) {
      this.comments[i].replies = allReplies[i];
    }
    this._cd.markForCheck();
  }

  private get id(): number {
    return this.bulletin ? this.bulletin.id : -1;
  }
}
