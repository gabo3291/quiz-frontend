import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizBulletinComponent } from './quiz-bulletin.component';

describe('QuizBulletinComponent', () => {
  let component: QuizBulletinComponent;
  let fixture: ComponentFixture<QuizBulletinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizBulletinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizBulletinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
