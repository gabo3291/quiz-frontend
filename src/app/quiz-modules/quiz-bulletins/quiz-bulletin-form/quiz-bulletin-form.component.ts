import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {AngularFireStorage} from '@angular/fire/storage';
import {AngularFirestore} from '@angular/fire/firestore';
import {QuizAttachmentModel, QuizBulletinPostModel} from '../../../quiz-models';
import {QuizBulletinsFakeService} from '../../../quiz-services/quiz-data-fake/quiz-bulletins-fake.service';
import {QuizBulletinImageService} from '../../../quiz-services/quiz-services';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {QuizBulletinsService} from '../../../quiz-services/quiz-api';

@Component({
  selector: 'quiz-bulletin-form',
  templateUrl: './quiz-bulletin-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class QuizBulletinFormComponent implements OnInit, OnDestroy {

  public formBulletin: FormGroup;
  public filesBulletins: string[] = [];
  public files: File[] = [];
  public filesToUpload: File[] = [];
  private _subscribe  = new Subject();

  constructor(
    public dialog: MatDialog,
    private _storage: AngularFireStorage,
    private _db: AngularFirestore,
    private _fb: FormBuilder,
    private _quizBulletinImage: QuizBulletinImageService,
    private _quizBulletinService: QuizBulletinsService,
    private _quizFakeBulletins: QuizBulletinsFakeService
  ) {
    this.formBulletin = this._fb.group({
      content: ['', [Validators.required]],
      file: ['']
    });
  }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this._subscribe.next();
    this._subscribe.complete();
  }


  public onFileChange(event: any): void {
   const FILES: FileList = event.target.files;
    // tslint:disable-next-line:prefer-for-of
   for (let i = 0; i < FILES.length; i++) {
     this.filesBulletins.push(FILES[i].name);
     this.files.push(FILES.item(i) as File);
   }
  }

  public initChargeOfData(): void {
    if (this.formBulletin.valid) {
      if (this.files.length === 0) {
        this.saveBulletin([]);
      }
      this._quizBulletinImage.totalImagesToCharge(this.files.length);
      this.filesToUpload = this.files;
    }
  }

  private init(): void {
    this._quizBulletinImage.listenIsLoadImages
      .pipe(takeUntil(this._subscribe))
      .subscribe((response: string[]) => {
        if (response) {
          this.saveBulletin(response);
        }
      });
  }
  private saveBulletin(fields: string[]): void {
    const BULLETIN_REQUEST: QuizBulletinPostModel = {
      content: this.formBulletin.value.content,
      fileIds: fields
    };
    this._quizBulletinService.saveBulletin(BULLETIN_REQUEST).subscribe(response => {
      this.dialog.closeAll();
      this._quizFakeBulletins.listenReloadData.next(true);
    });
  }
}
