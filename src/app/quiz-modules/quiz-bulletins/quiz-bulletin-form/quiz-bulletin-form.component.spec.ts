import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizBulletinFormComponent } from './quiz-bulletin-form.component';

describe('QuizBulletinFormComponent', () => {
  let component: QuizBulletinFormComponent;
  let fixture: ComponentFixture<QuizBulletinFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizBulletinFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizBulletinFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
