import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizBulletinsListComponent } from './quiz-bulletins-list.component';

describe('QuizBulletinsComponent', () => {
  let component: QuizBulletinsListComponent;
  let fixture: ComponentFixture<QuizBulletinsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [QuizBulletinsListComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizBulletinsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
