import {Component, Input, ViewEncapsulation} from '@angular/core';
import { QuizBulletinResponseModel} from '../../../quiz-models';

@Component({
  selector: 'quiz-bulletins-list',
  templateUrl: './quiz-bulletins-list.component.html',
  encapsulation: ViewEncapsulation.None
})
export class QuizBulletinsListComponent {
  @Input() bulletins: QuizBulletinResponseModel[] = [];
  constructor() { }
}
