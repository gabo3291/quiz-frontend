import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizReplyCommentFormComponent } from './quiz-reply-comment-form.component';

describe('QuizReplyCommentFormComponent', () => {
  let component: QuizReplyCommentFormComponent;
  let fixture: ComponentFixture<QuizReplyCommentFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizReplyCommentFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizReplyCommentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
