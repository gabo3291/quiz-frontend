import {ChangeDetectionStrategy, Component, ElementRef, Inject, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuizCommentsService} from '../../../quiz-services/quiz-api';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {QuizCommentResponseModel} from '../../../quiz-models';

@Component({
  selector: 'quiz-quiz-reply-comment-form',
  templateUrl: './quiz-reply-comment-form.component.html',
  encapsulation: ViewEncapsulation.None
})
export class QuizReplyCommentFormComponent {
  @ViewChild('passData') passData: ElementRef | undefined;
  public formReply: FormGroup;
  public savedReplyComment: QuizCommentResponseModel | undefined;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {id: number},
    private _fb: FormBuilder,
    private _quizCommentService: QuizCommentsService
  ) {
    this.formReply = this._fb.group({
      content: ['', Validators.required]
    });
  }

  public saveReply(): void {
    if (this.formReply.valid) {
      this._quizCommentService.saveReply(this.formReply.value.content, this.data.id)
        .subscribe((response: QuizCommentResponseModel) => {
          this.savedReplyComment = response;
          setTimeout(() => {
            if (this.passData) {
              this.passData.nativeElement.click();
            }
          }, 500);
        });
    }
  }

}
