import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';

import {QuizBulletinsComponent} from './quiz-bulletins.component';
import {QuizSharedModule} from '../quiz-shared';
import {QuizBulletinsListComponent} from './quiz-bulletins-list/quiz-bulletins-list.component';
import {QuizBulletinComponent} from './quiz-bulletin/quiz-bulletin.component';
import {QuizBulletinImagesComponent} from './quiz-bulletin-images/quiz-bulletin-images.component';
import {QuizBulletinFormComponent} from './quiz-bulletin-form/quiz-bulletin-form.component';
import {QuizBulletinUploadTaskComponent} from './quiz-bulletin-upload-task/quiz-bulletin-upload-task.component';
import {QuizReplyCommentFormComponent} from './quiz-reply-comment-form/quiz-reply-comment-form.component';

const routes: Routes = [
  {
    path: '',
    component: QuizBulletinsComponent
  }
];

@NgModule({
  declarations: [
    QuizBulletinsComponent,
    QuizBulletinsListComponent,
    QuizBulletinComponent,
    QuizBulletinImagesComponent,
    QuizBulletinFormComponent,
    QuizReplyCommentFormComponent,
    QuizBulletinUploadTaskComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    QuizSharedModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ],
  exports: [RouterModule]
})
export class QuizBulletinsRoutingModule { }
