import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizLoginFormComponent } from './quiz-login-form.component';

describe('QuizLoginComponent', () => {
  let component: QuizLoginFormComponent;
  let fixture: ComponentFixture<QuizLoginFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizLoginFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizLoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
