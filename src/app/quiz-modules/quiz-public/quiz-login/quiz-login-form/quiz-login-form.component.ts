import {Component, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuizRegisterService} from '../../../../quiz-services/quiz-api';

@Component({
  selector: 'quiz-login-form',
  templateUrl: './quiz-login-form.component.html',
  encapsulation: ViewEncapsulation.None
})
export class QuizLoginFormComponent {
  public formLogin: FormGroup;
  constructor(private _fb: FormBuilder, private _registerService: QuizRegisterService) {
    this.formLogin = this._fb.group({
      accountId: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  public submitLogin(): void {
    if (this.formLogin.valid) {
      console.log(this.formLogin.value);
    }
  }


}
