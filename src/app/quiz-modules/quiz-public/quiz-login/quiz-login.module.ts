import { NgModule } from '@angular/core';
import { QuizRegisterService } from '../../../quiz-services/quiz-api';
import {QuizLoginRoutingModule} from './quiz-login-routing.module';

@NgModule({
  imports: [
    QuizLoginRoutingModule
  ],
  providers: [
    QuizRegisterService
  ]
})
export class QuizLoginModule { }
