import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {QuizLoginComponent} from './quiz-login.component';
import {MatInputModule} from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import {ReactiveFormsModule} from '@angular/forms';
import {QuizRegisterFormComponent} from './quiz-register-form/quiz-register-form.component';
import {QuizLoginFormComponent} from './quiz-login-form/quiz-login-form.component';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: QuizLoginComponent
  }
];

@NgModule({
  declarations: [
    QuizLoginComponent,
    QuizLoginFormComponent,
    QuizRegisterFormComponent
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatTabsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class QuizLoginRoutingModule { }
