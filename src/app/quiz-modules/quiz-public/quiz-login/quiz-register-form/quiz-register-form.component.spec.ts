import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizRegisterFormComponent } from './quiz-register-form.component';

describe('QuizRegisterComponent', () => {
  let component: QuizRegisterFormComponent;
  let fixture: ComponentFixture<QuizRegisterFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizRegisterFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizRegisterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
