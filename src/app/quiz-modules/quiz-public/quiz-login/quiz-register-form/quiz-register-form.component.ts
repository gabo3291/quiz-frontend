import { Component } from '@angular/core';
import {Router} from '@angular/router';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuizRegisterService} from '../../../../quiz-services/quiz-api';
import {QuizUserModel} from '../../../../quiz-models';

@Component({
  selector: 'quiz-register-form',
  templateUrl: './quiz-register-form.component.html'
})
export class QuizRegisterFormComponent {

  public formRegister: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _registerService: QuizRegisterService
  ) {
    this.formRegister = this._fb.group({
      accountId: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  public submitRegister(): void {
    if (this.formRegister.valid) {
      const { accountId, firstName, lastName } = this.formRegister.value;
      const USER: QuizUserModel = {
        accountId, firstName, lastName,
        createdDate: new Date().toISOString(),
        isDeleted: false
      };
      this._registerService.createUser(USER).subscribe((response: QuizUserModel) => {
        localStorage.setItem('user', JSON.stringify(response));
        this._router.navigate(['bulletins']);
      });
    }
  }

}
