import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'quiz-quiz-login-page',
  templateUrl: './quiz-login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class QuizLoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
