import { NgModule } from '@angular/core';
import {QuizNavbarComponent} from './quiz-navbar/quiz-navbar.component';
import {QuizSidebarComponent} from './quiz-sidebar/quiz-sidebar.component';

@NgModule({
  declarations: [
    QuizNavbarComponent,
    QuizSidebarComponent
  ],
  exports: [
    QuizNavbarComponent,
    QuizSidebarComponent
  ]
})
export class QuizSharedModule { }
