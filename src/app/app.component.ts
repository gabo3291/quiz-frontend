import { Component } from '@angular/core';

@Component({
  selector: 'quiz-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  title = 'quiz-frontend-ng';
}
