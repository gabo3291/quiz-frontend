import { TestBed } from '@angular/core/testing';

import { QuizBulletinsService } from './quiz-bulletins.service';

describe('QuizBulletinsService', () => {
  let service: QuizBulletinsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuizBulletinsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
