import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {QuizBulletinPostModel, QuizBulletinResponseModel} from '../../../quiz-models';

@Injectable()
export class QuizBulletinsService {

  constructor(private _http: HttpClient) { }

  public getAllBulletins(): Observable<QuizBulletinResponseModel[]> {
    return this._http.get<QuizBulletinResponseModel[]>(`${environment.urlBulletinsApi}/bulletins`);
  }

  public getBulletins(): Observable<QuizBulletinResponseModel[]> {
    const headers = new HttpHeaders({
      'Account-ID': '77',
      'User-ID': '13'
    });
    return this._http.get<QuizBulletinResponseModel[]>(`${environment.urlBulletinsApi}/bulletins/pagination?limit=12&page=1`, {headers});
  }

  public saveBulletin(data: QuizBulletinPostModel): Observable<any> {
    const headers = new HttpHeaders({
      'Account-ID': '177',
      'User-ID': '113'
    });
    return this._http.post(`${environment.urlBulletinsApi}/bulletins/`, data, {headers});
  }
}
