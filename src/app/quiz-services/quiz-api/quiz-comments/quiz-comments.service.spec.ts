import { TestBed } from '@angular/core/testing';

import { QuizCommentsService } from './quiz-comments.service';

describe('QuizCommentsService', () => {
  let service: QuizCommentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuizCommentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
