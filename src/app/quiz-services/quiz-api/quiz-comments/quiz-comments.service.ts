import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {environment} from '../../../../environments/environment';
import {QuizCommentResponseModel, QuizUserModel} from '../../../quiz-models';
import {QuizCommentRequestModel} from '../../../quiz-models/quiz-comment-model';

@Injectable()
export class QuizCommentsService {

  constructor(private _http: HttpClient) {
  }

  public getCommentsByBulletinId(bulletinId: number): Observable<QuizCommentResponseModel[]> {
    return this._http.get<QuizCommentResponseModel[]>(`${environment.urlBulletinsApi}/comments/${bulletinId}`);
  }

  public getRepliesByCommentId(commentId: number): Observable<QuizCommentResponseModel[]> {
    return this._http.get<QuizCommentResponseModel[]>(`${environment.urlBulletinsApi}/comments/${commentId}/replies`);
  }

  public saveComment(content: string, bulletinId: number): Observable<QuizCommentResponseModel> {
    let headers;
    const userLocal = localStorage.getItem('user');
    let user: QuizUserModel;
    let request: QuizCommentRequestModel;

    user = JSON.parse(userLocal ? userLocal : '');
    request = {
      content,
      senderUserId: user.accountId
    };
    headers = new HttpHeaders({
      'Account-ID': user.accountId + '',
      'Bulletin-ID': bulletinId + ''
    });

    return this._http.post<QuizCommentResponseModel>(`${environment.urlBulletinsApi}/comments`, request, {headers});
  }

  public saveReply(content: string, commentId: number): Observable<QuizCommentResponseModel> {
    const userLocal = localStorage.getItem('user');
    const user: QuizUserModel = JSON.parse(userLocal ? userLocal : '');
    const request: QuizCommentRequestModel = {
      content,
      senderUserId: user.accountId
    };
    return  this._http.post<QuizCommentResponseModel>(`${environment.urlBulletinsApi}/comments/${commentId}/replies`, request);
  }
}
