import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from '../../../../environments/environment';

import {QuizUserModel} from '../../../quiz-models';

@Injectable()
export class QuizRegisterService {

  constructor(private _http: HttpClient) { }

  public createUser(user: QuizUserModel): Observable<QuizUserModel> {
    return this._http.post<QuizUserModel>(environment.urlUsersApi, user);
  }
}
