import { TestBed } from '@angular/core/testing';

import { QuizRegisterService } from './quiz-register.service';

describe('QuizRegisterService', () => {
  let service: QuizRegisterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuizRegisterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
