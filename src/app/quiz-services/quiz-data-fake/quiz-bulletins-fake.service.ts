import { Injectable } from '@angular/core';
import {DATA_FAKE} from './quiz-bulletins-fake';
import {BehaviorSubject, Subject} from 'rxjs';
import {QuizBulletinResponseModel} from '../../quiz-models';


@Injectable()
export class QuizBulletinsFakeService {

  public bulletins$;
  public listenReloadData: BehaviorSubject<boolean>;
  private _fakeData: QuizBulletinResponseModel[];
  private _bulletinsSubject: BehaviorSubject<QuizBulletinResponseModel[]>;

  constructor() {
    this._fakeData = [];
    this._bulletinsSubject = new BehaviorSubject<QuizBulletinResponseModel[]>(this._fakeData);
    this.bulletins$ = this._bulletinsSubject.asObservable();
    this.listenReloadData = new BehaviorSubject<boolean>(true);
  }

  /**
   * Only frontend
   */
  public addBulletin(bulletin: QuizBulletinResponseModel): void {
    this._fakeData.push(bulletin);
    this._bulletinsSubject.next(this._fakeData);
  }

  public updateListBulletins(bulletins: QuizBulletinResponseModel[]): void {
    this._fakeData = bulletins;
    this._bulletinsSubject.next(this._fakeData);
  }
}
