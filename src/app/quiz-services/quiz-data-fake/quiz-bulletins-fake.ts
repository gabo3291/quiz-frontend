
export const DATA_FAKE: any[] = [
  {
    bulletinId: 0,
    accountId: 0,
    commentId: 0,
    body: '10 of the worlds biggest superyachts 1',
    createdDate: '2020-05-24T22:58:06.915Z',
    isDeleted: true,
    repliesCounter: 0,
    senderUser: {
      name1: 'string',
      name2: 'string',
      account: 'String',
      userId: 0,
    },
    attachments: [
      {
        fileId: 'String',
        size: 0,
        name: 'String',
        mimeType: 'String',
      },
    ],
  },
  {
    bulletinId: 0,
    accountId: 0,
    commentId: 0,
    body: '\n' +
      'Valtteri Bottas backs teammate Lewis Hamilton to triumph in F1 championship fight',
    createdDate: '2020-05-24T22:58:06.915Z',
    isDeleted: true,
    repliesCounter: 0,
    senderUser: {
      name1: 'string',
      name2: 'string',
      account: 'String',
      userId: 0,
    },
    attachments: [
      {
        fileId: 'String',
        size: 0,
        name: 'String',
        mimeType: 'String',
      },
    ],
  },
];
