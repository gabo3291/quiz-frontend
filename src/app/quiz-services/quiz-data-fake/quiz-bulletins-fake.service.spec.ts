import { TestBed } from '@angular/core/testing';

import { QuizBulletinsFakeService } from './quiz-bulletins-fake.service';

describe('QuizBulletinsService', () => {
  let service: QuizBulletinsFakeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuizBulletinsFakeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
