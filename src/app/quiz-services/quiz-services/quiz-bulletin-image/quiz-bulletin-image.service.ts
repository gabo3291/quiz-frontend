import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class QuizBulletinImageService {

  public listenIsLoadImages: Subject<string[]>;
  private stateNameOfImages: string[];
  private readonly stateOfImages: { quantity: number, quantityLoaded: number };

  constructor() {
    this.listenIsLoadImages = new Subject<string[]>();
    this.stateNameOfImages = [];
    this.stateOfImages = {quantity: 0, quantityLoaded: 0};
  }

  public totalImagesToCharge(quantity: number): void {
    this.stateOfImages.quantity = quantity;
  }

  public imageUploadSuccessful(nameImage: string): void {
    this.stateOfImages.quantityLoaded++;
    this.stateNameOfImages.push(nameImage);
    const { quantity, quantityLoaded } = this.stateOfImages;
    if (quantity === quantityLoaded) {
      this.listenIsLoadImages.next(this.stateNameOfImages);
      this.resetStateOfImages();
    }
  }
  private resetStateOfImages(): void {
    this.stateOfImages.quantity = 0;
    this.stateOfImages.quantityLoaded = 0;
    this.stateNameOfImages = [];
  }
}
