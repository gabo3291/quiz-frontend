import { TestBed } from '@angular/core/testing';

import { QuizBulletinImageService } from './quiz-bulletin-image.service';

describe('QuizBulletinImageService', () => {
  let service: QuizBulletinImageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuizBulletinImageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
