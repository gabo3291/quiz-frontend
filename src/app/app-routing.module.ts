import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./quiz-modules/quiz-public/quiz-login/quiz-login.module').then((m) => m.QuizLoginModule)
  },
  {
    path: 'bulletins',
    loadChildren: () =>
      import('./quiz-modules/quiz-bulletins/quiz-bulletins.module').then((m) => m.QuizBulletinsModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
