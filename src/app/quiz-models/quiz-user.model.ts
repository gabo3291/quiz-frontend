export interface QuizUserModel {
  id?: string;
  accountId: number;
  firstName: string;
  lastName: string;
  createdDate: string;
  isDeleted: boolean;
}
