export interface QuizSenderUserModel {
  name1: string;
  name2: string;
  account: string;
  userId: number;
}
