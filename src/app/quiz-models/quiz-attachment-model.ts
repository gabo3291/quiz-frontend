export interface QuizAttachmentModel {
  fileId: string;
  size: number;
  name: string;
  mimeType: string;
}

export interface QuizAttachmentResponseModel {
  id: number;
  accountId: number;
  field: string;
  createdDate: Date;
  isDeleted: boolean;
}
