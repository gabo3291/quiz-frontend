export interface QuizCommentResponseModel {
  id: number;
  accountId: number;
  bulletinId: number;
  senderUserId: number;
  content: string;
  repliesCounter: number;
  createdDate: Date;
  isDeleted: boolean;
  replies?: QuizCommentResponseModel[];
}
export interface QuizCommentRequestModel {
  senderUserId: number;
  content: string;
}
