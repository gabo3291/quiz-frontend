import { QuizAttachmentResponseModel } from './quiz-attachment-model';

export interface QuizBulletinPostModel {
  content: string;
  fileIds: string[];
}

export interface QuizBulletinResponseModel {
  id: number;
  accountId: number;
  senderUserId: number;
  body: string;
  createdDate: Date;
  isDeleted: boolean;
  commentsCounter: number;
  attachments: QuizAttachmentResponseModel[];
}
